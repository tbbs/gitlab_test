
#  @color[gray](A brief) @color[orange](Gitlab) introduction

@size[40%](link: https://gitpitch.com/tbbs/gitlab_test/master?grs=gitlab&t=black)


#HSLIDE

@transition[zoom-in fade-in]
## Why Gitlab?

- Code versionning
- Merge code from different sources
- Comment changes online
- Track issue
- Plan and organize your work
- Communicate

+++
@transition[zoom-in fade-in]
## Old fashion versioning
![issueboard image](images/badVersioning.PNG) 

+++
@transition[zoom-in fade-in]
# Merging code
@transition[zoom-in fade-in]
Merging files by hand when working with multiple people on same project





#HSLIDE

@transition[zoom-in fade-in]
# Git workflow
@transition[zoom-in fade-in]
+++?image=images/gitdashflow.png&size=30%
@transition[zoom-in fade-in]
+++?image=images/environment_branches.png&size=30%

+++
# Track changes
![](images/TrackChanges.PNG)

+++
# Comment changes
![](images/changesComment.PNG)

+++
# Example

Link: https://gitlab.com/tbbs/gitlab_test/network/master






#HSLIDE

# IssueBoard
![issueboard image](images/issue-board.gif) 
